import 'package:flutter/material.dart';
import 'package:nelson_moloi_module_4/screens/home/home.dart';

Future void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(const MyApp());
}

Future initialization(build(BuildContext? context)) async {
  await Future.delayed(Duration(seconds:5));
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: Home(),
    );
  }
}